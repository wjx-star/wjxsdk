# 在您的App中使用弹窗的方式打开问卷星的问卷

## SDK介绍
本仓库提供的SDK支持以下效果：作答者在App中打开问卷时，不跳转新的页面，而是在现有的页面上，弹出一个半屏弹窗，在这个半屏弹窗上可以呈现一个或多个题目。

此SDK最终还是以Webview的方式打开的问卷，与打开一个全新的Webview相比，以半屏弹窗打开，可以不干扰用户当前在App中的操作流程。
| 对比项  | 全屏新页面打开    | 弹窗SDK打开   |
|------|------------|-----------|
| 技术方案 | 新页面Webview | 弹窗Webview |
| 和现有页面关系     | 打开的新页面覆盖了现有页面           | 打开的页面是在现有页面上以弹窗形式打开的          |


## 面向人群
本SDK面相的人群是App开发者，并且需要在自己开发的App中，以弹窗的形式打开问卷星平台的问卷。

## SDK可以传入两个参数：
1. 要打开问卷的URL地址；
> - 这里传入的URL就是普通的一个URL，不见的一定是问卷星的问卷URL;
> - 传入URL时，如果携带参数，可以传入作答者的ID，可参考问卷星的文档：[自定义连接参数](https://www.wjx.cn/Help/Help.aspx?helpid=19)
> - 传入URL时，可以通过参数来控制以「一题一页」的样式来作答，还是以「默认的多题一页」的样式来作答，详情可联系服务顾问。

2. 竖屏弹窗时，弹窗高度占屏幕整体高度的百分比，默认值为：60%，合法取值范围在40%到80%之间的整数值；
3. 横屏弹窗时，弹窗宽度占屏幕整体宽度的百分比，默认值为：50%，合法取值范围在50%到80%之间的整数值。

## 不同开发环境的SDK
- iOS端Swift开发语言：[https://gitee.com/wjx-star/wjxsdk/tree/swift/](https://gitee.com/wjx-star/wjxsdk/tree/swift/)
- iOS端Objective-C开发语言：[https://gitee.com/wjx-star/wjxsdk/tree/oc/](https://gitee.com/wjx-star/wjxsdk/tree/oc/)
- 安卓端Java语言：[https://gitee.com/wjx-star/wjxsdk/tree/android/](https://gitee.com/wjx-star/wjxsdk/tree/android/)
